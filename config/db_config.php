<?php
try{
    $db = new PDO('mysql:host=127.0.0.1:3306;dbname=ok309080_monitoring', 'admin', 'admin');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec('SET NAMES "utf8"');

} catch(Exception $error) {
    $output = 'Technical problems, please come later :)' . $error->getMessage();
    die($output);
}
?>
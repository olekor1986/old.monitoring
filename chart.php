<?php
require_once "weatherOdessaLoader.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ChartView</title>
    <style>
        .content {
            text-align: center;
        }
        #chartdiv {
            width: 90%;
            height: 600px;
        }
    </style>

    <!-- Resources -->
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <script>
        am4core.ready(function() {


// Themes begin
            am4core.useTheme(am4themes_material);
            am4core.useTheme(am4themes_animated);
// Themes end

            var chart = am4core.create("chartdiv", am4charts.XYChart);

// Set up data source
            chart.dataSource.url = "chartPrim59.csv";
            chart.dataSource.parser = new am4core.CSVParser();
            chart.dataSource.parser.options.useColumnNames = true;

// Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.labels.template.fontSize = 14;
            categoryAxis.renderer.labels.template.rotation = 90;
            //categoryAxis.renderer.labels.template.disabled = true;


// Create value axis
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = "Температура, °C";
            valueAxis.renderer.labels.template.fontSize = 14;


// Create series
            var series1 = chart.series.push(new am4charts.LineSeries());
            series1.dataFields.valueY = "t1";
            series1.dataFields.categoryX = "date";
            series1.name = "Подача";
            series1.strokeWidth = 3;
            series1.stroke = am4core.color("#cd2a01");
            series1.tensionX = 0.9;
            series1.tensionY = 0.9;

            var series2 = chart.series.push(new am4charts.LineSeries());
            series2.dataFields.valueY = "t2";
            series2.dataFields.categoryX = "date";
            series2.name = "Обратка";
            series2.strokeWidth = 3;
            series2.stroke = am4core.color("#ac39cd");
            series2.tensionX = 0.9;
            series2.tensionY = 0.9;
        /*
            var series3 = chart.series.push(new am4charts.LineSeries());
            series3.dataFields.valueY = "tn";
            series3.dataFields.categoryX = "date";
            series3.name = "Наружный воздух";
            series3.strokeWidth = 1;
            series3.stroke = am4core.color("#ac39cd");
            series3.tensionX = 0.7;
            series3.tensionY = 0.7;
        */
// Add legend
            chart.legend = new am4charts.Legend();
            chart.scrollbarX = new am4core.Scrollbar();
            chart.cursor= new am4charts.XYCursor();


        }); // end am4core.ready()
    </script>

</head>
<body>
<div class="content">
    <h2>Приморская, 59</h2>
    <div id="chartdiv"></div>
    <p>Температура наружного воздуха <snap style="color:blue"><?=$currentWeatherOutTemp?></snap> °C
        (по состоянию на <snap style="color:darkgreen"><?=$currentWeatherDate?></snap>)</p>
    <form action="displayParam.php" method="GET">
        <label for="number">Значения</label>
        <input id="number" type="text" name="num">
        <button>Показать</button>
    </form>
</div>
</body>
</html>

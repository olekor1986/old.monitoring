<?php
define("W_CITY_ID", '698740');
define("W_API_KEY", '1fecc058008486963b366cef45a8e287');
define("W_API_URL", 'https://api.openweathermap.org/data/2.5/weather?id=');

if (file_get_contents(W_API_URL . W_CITY_ID . '&appid=' . W_API_KEY)) {
    $currentWeatherJSON = file_get_contents(W_API_URL . W_CITY_ID . '&appid=' . W_API_KEY);
} else {
    $currentWeatherOutTemp = $currentWeatherDate = 'Unknown error';
}
$weatherArray = json_decode($currentWeatherJSON, true);

$currentWeatherDate = $weatherArray['dt'];
$currentWeatherDate = date("Y-m-d H:i:s", $currentWeatherDate);
$currentWeatherOutTemp = $weatherArray['main']['temp'] - 273;
?>
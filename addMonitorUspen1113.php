<?php
require_once "weatherOdessaLoader.php";
require_once "config/db_config.php";



define('TELEGRAM_TOKEN', '1025369841:AAE0P60FRIGsxoGnniaiwnF4JDetJnEW60M');
//define('TELEGRAM_CHATID', '620175323');
//$chatId_1 = '620175323';
//$chatId_2 = '1091130220';
$chatIdOkovitij = '608695270';
$chatIdShalar = '1491741201';
$chatIdSkrypnik = '1488538863';
$condition = true;
define('T1_LOW', '30');
define('T1_HIGH', '60');
define('TV_LOW', '5');
define('TV_HIGH', '38');
define('EQ_ERR', '!!!---АВАРИЯ ОБОРУДОВАНИЯ---!!!');
define('NORM', '(((ВСЕ В НОРМЕ)))');
define('ADDRESS', 'Успенская, 11/13');

$source_data = json_encode($_GET);

$error_log = fopen("uspen1113_error_log.csv", "a");
fwrite($error_log, $source_data);
fclose($error_log);

unset($source_data);

function message_to_telegram($text, $chatId)
{
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendMessage',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_POSTFIELDS => array(
                'chat_id' => $chatId,
                'text' => $text,
            ),
        )
    );
    curl_exec($ch);
}

if (!empty($_GET)) {
    foreach ($_GET as $value) {
        if ($value == -127 || $value == 85) {
            die(0);
        }

    }
} else {
    die(0);
}

$k = htmlspecialchars(stripslashes(trim($_GET['k'])), ENT_QUOTES, "UTF-8");


if ($k != 'p74f202bd0u3v23tr') {
    die(0);
}

$t_1 = htmlspecialchars(stripslashes(trim($_GET['t0'])), ENT_QUOTES, "UTF-8");
$t_2 = htmlspecialchars(stripslashes(trim($_GET['t1'])), ENT_QUOTES, "UTF-8");
$t_v = htmlspecialchars(stripslashes(trim($_GET['t2'])), ENT_QUOTES, "UTF-8");
if ($t_v == $t_1 || $t_v == $t_2 || $t_1 == $t_2) {
    die(0);
}


$t_n = round($currentWeatherOutTemp, 2);

if ($t_n == -273) {
    die(0);
}


$alert = '';
$message = 'T1=' . $t_1 . '°C; ' . "\r\n"
        . 'T2=' . $t_2 . '°C; ' . "\r\n"
        . 'Tв=' . $t_v . '°C; ' . "\r\n"
        . 'Tн=' . $t_n . '°C;';

if ($t_1 < T1_LOW) {
    $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
            "Низкая температура подачи  - " . $t_1 . '°C';
    message_to_telegram($alert . "\r\n" . $message, $chatIdOkovitij);
    message_to_telegram($alert . "\r\n" . $message, $chatIdShalar);
    message_to_telegram($alert . "\r\n" . $message, $chatIdSkrypnik);
} else if ($t_1 > T1_HIGH) {
    $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
            "Высокая температура подачи  - " . $t_1 . '°C';
    message_to_telegram($alert . "\r\n" . $message, $chatIdOkovitij);
    message_to_telegram($alert . "\r\n" . $message, $chatIdShalar);
    message_to_telegram($alert . "\r\n" . $message, $chatIdSkrypnik);
} else {
    $condition = false;
}

if ($t_v < TV_LOW) {
    $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
            "Низкая температура помещения  - " . $t_v . '°C';
    message_to_telegram($alert . "\r\n" . $message, $chatIdOkovitij);
    message_to_telegram($alert . "\r\n" . $message, $chatIdShalar);
    message_to_telegram($alert . "\r\n" . $message, $chatIdSkrypnik);
} else if ($t_v > TV_HIGH) {
    $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
            "Высокая температура помещения  - " . $t_v . '°C';
    message_to_telegram($alert . "\r\n" . $message, $chatIdOkovitij);
    message_to_telegram($alert . "\r\n" . $message, $chatIdShalar);
    message_to_telegram($alert . "\r\n" . $message, $chatIdSkrypnik);
} else {
    $condition = false;
}


$date = new DateTime('now');
$data = '';
$data .= $date->format('Y-m-d H:i:s') . ',';
$data .= $t_1 . ',';
$data .= $t_2 . ',';
$data .= $t_v . ',';
$data .= $t_n;
$data .= "\r\n";

$message = ADDRESS . "\r\n" . NORM . "\r\n" .  $message;
if ($condition == true){
       if (!file_exists('timelog1113.txt')) {
        message_to_telegram($message, $chatIdOkovitij);
        message_to_telegram($message, $chatIdShalar);
        message_to_telegram($message, $chatIdSkrypnik);
        $timelog = fopen("timelog1113.txt", "w");
        fwrite($timelog, $date->getTimestamp());
        fclose($timelog);
    } else {
        $timestamp = file_get_contents('timelog1113.txt');
        $timeLast = new DateTime();
        $timeLast = $timeLast->setTimestamp($timestamp);
        $interval = $date->diff($timeLast);
        if ($interval->{'h'} >= 1) {
            message_to_telegram($message, $chatIdOkovitij);
            message_to_telegram($message, $chatIdShalar);
            message_to_telegram($message, $chatIdSkrypnik);
            $timelog = fopen("timelog1113.txt", "w");
            fwrite($timelog, $date->getTimestamp());
            fclose($timelog);
        }
    }
}




$file = fopen("uspen1113Log20-21.csv", "a");
fwrite($file, $data);
fclose($file);

try {
    $sql = "INSERT INTO monitoring_uspen1113 SET
            t_1 = :t_1,
            t_2 = :t_2,
            t_v = :t_v,
            t_n = :t_n
            ";
    $storeObject = $db->prepare($sql);
    $storeObject->bindValue(':t_1', $t_1);
    $storeObject->bindValue(':t_2', $t_2);
    $storeObject->bindValue(':t_v', $t_v);
    $storeObject->bindValue(':t_n', $t_n);

    $storeObject->execute();
} catch (Exception $e) {
    $message = 'Error adding data! ' . $e->getMessage();
    die($message);
}
unset($condition);
die(1);
?>
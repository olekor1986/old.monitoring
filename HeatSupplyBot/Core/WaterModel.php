<?php


/**
 * Class WaterModel
 */
class WaterModel extends DB
{
    /**
     * @var int
     */
    protected $limit = 5;
    /**
     * @var string
     */
    protected $tableName;
    /**
     * @var string
     */
    protected $field = 'data';
    /**
     * @var string
     */
    protected $title;

    /**
     * Model constructor.
     * @param $title
     * @param $tableName
     * @param $field
     */
    public function __construct($title, $tableName, $field)
    {
        parent::__construct();
        $this->title = $title;
        $this->tableName = $tableName;
        $this->field = $field;

    }

    /**
     * @return string
     */
    public function getLastData():string
    {
        $tableName = $this->getTableName();
        $field = $this->getField();
        try {
            $sql = "SELECT * FROM " . "$tableName" . " ORDER BY " . "$tableName" . "." . "$field" . " DESC LIMIT 1";
            $result = $this->db->query($sql);
            $data = $result->fetch();

            $level = '';
            if($data['level'] == 0){
                $level = 'Уровень в норме';
            } else {
                $level = 'Аварийный уровень!';
            }
            $string = $this->title . "\r\n"
                    . $data['date'] . ':' . "\r\n"
                    . 'T1=' . $data['t1'] . '°C; ' . "\r\n"
                    . 'T2=' . $data['t2'] . '°C; ' . "\r\n"
                    . 'P1=' . round($data['p1'], 3) . 'bar ' . "\r\n"
                    . 'P2=' . round($data['p2'], 3) . 'bar ' . "\r\n"
                    . $level;
            return $string;
           
        } catch(Exception $e) {
            return 'Error getting data!' . $e->getMessage();
        }
    }

   
    /**
     * @param string $title
     * @return string
     */
    public function Transliterate(string $title):string
    {
        $alf = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',    'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        $title = strtr($title, $alf);
        $title = mb_strtolower($title);
        $title = mb_ereg_replace('[^-0-9a-z]', '_', $title);
        $title = mb_ereg_replace('[-]+', '_', $title);
        $title = trim($title, '_');
        return $title;
    }
    /**
     * @return string
     */
    public function getTableName():string
    {
        return $this->tableName;
    }

    /**
     * @return string
     */
    public function getField():string
    {
        return $this->field;
    }

    /**
     * @return int
     */
    public function getLimit():int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return mixed
     */
    public function getTitle():string
    {
        return $this->title;
    }
}
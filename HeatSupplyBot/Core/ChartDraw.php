<?php


/**
 * Class ChartDraw
 */
class ChartDraw
{
    /**
     * @var Model
     */
    protected $source;
    /**
     * @var int
     */
    protected $width = 500;
    /**
     * @var int
     */
    protected $height = 500;


    /**
     * ChartDraw constructor.
     * @param Model $source
     * @param int $width
     * @param int $height
     */
    public function __construct(Model $source, int $width, int $height)
    {
        $this->width = $width;
        $this->height = $height;
        $this->source = $source;
    }

    /**
     * @param array $array
     * @return string
     */
    public function DrawLinearChart(array $array):string
    {
        function ImageColor($im, $color_array)
        {
            return ImageColorAllocate(
                $im,
                isset($color_array['r']) ? $color_array['r'] : 0,
                isset($color_array['g']) ? $color_array['g'] : 0,
                isset($color_array['b']) ? $color_array['b'] : 0
            );
        }
        $array = array_reverse($array);
        $DATA = [];
        foreach ($array as $value){
            $DATA[0][] = floatval($value['t_1']);
            $DATA[1][] = floatval($value['t_2']);
            $DATA['x'][] = $value['date'];
        }

        $title = $this->source->getTitle();
        $width = $this->width;
        $height  = $this->height;
        $padding = 20;
        $step = 2;
        $count = 10;

        $im = @ImageCreate ($width, $height)
        or die ("Cannot Initialize new GD image stream");

        $bgcolor = ImageColor($im, array('r'=>255, 'g'=>255, 'b'=>255));
        $color = ImageColor($im, array('b'=>175));
        $green = ImageColor($im, array('g'=>175));
        $gray = ImageColor($im, array('r'=>175, 'g'=>175, 'b'=>175));
        $red = ImageColor($im, array('r'=>232, 'g'=>9, 'b'=>9));
        $purple = ImageColor($im, array('r'=>160, 'g'=>9, 'b'=>232));
        $black = ImageColor($im, array('r'=>0, 'g'=>0, 'b'=>0));


        $graphWidth  = $width - 2 * $padding;
        $graphHeight = $height - 2 * $padding - 120;


        $max = max(max($DATA[0]), max($DATA[1]));
        $min = min(min($DATA[0]), min($DATA[1]));
        $min = floor($min/$step) * $step;
        $max = ceil($max/$step) * $step;


        for($i = $min; $i < $max + $step; $i += $step)
        {
            $y = $graphHeight - ($i - $min) * ($graphHeight) / ($max - $min) + $padding;
            ImageLine($im, $padding, $y, $graphWidth + $padding, $y, $gray);
            ImageTTFText($im, 8, 0, $padding - 15, $y - 1, $black,
                "./fonts/verdana.ttf", $i);
        }

        $y1 = $graphHeight + $padding;

        for($i = 0; $i < $count; $i++)
        {
            $x1 = $x2 = $padding + $i * ($graphWidth / $count);
            $y1 = $graphHeight + $padding;
            $y2 = $y1 - $graphHeight;
            ImageLine($im, $x1, $y1, $x2, $y2, $gray);
        }

        $dataCount = count($DATA['x']);
        $fontWidth=imagefontwidth(2);
        $x1 = $padding;
        $prev=10000;
        $textWidth=$fontWidth*strlen($DATA["x"][0])+6;
        $i=$x1+$graphWidth;

        while ($i>$x1) {
            if ($prev-$textWidth>$i) {
                $drawx=$i-($graphWidth/$dataCount)/2;
                if ($drawx>$x1) {
                    $str=$DATA["x"][round(($i-$x1)/($graphWidth/$dataCount))-1];
                    imageline($im,$drawx,$y1,$i-($graphWidth/$dataCount)/2,$y1+5,$black);
                    ImageTTFText($im, 8, -90, $drawx - 2, $y1 + 10, $black,
                        "./fonts/verdana.ttf", $str);
                }
                $prev=$i;
            }
            $i-=$graphWidth/$dataCount;
        }

        $cnt = count($DATA[0]);
        $x22 = $x2 = $padding;
        $i  = 0;

        $y2 = $graphHeight - ($DATA[0][$i] - $min) * ($graphHeight) / ($max - $min) + $padding;
        $y22 = $graphHeight - ($DATA[1][$i] - $min) * ($graphHeight) / ($max - $min) + $padding;

        for($i = 1; $i < $cnt; $i++)
        {
            $x1 = $x2;
            $x2 = $x1 + (($graphWidth) / ($cnt - 1));
            $y1 = $y2;
            $y2 = $graphHeight - ($DATA[0][$i] - $min) * ($graphHeight) / ($max - $min) + $padding;

            ImageLine($im, $x1, $y1, $x2, $y2, $red);
            ImageLine($im, $x1 + 1, $y1, $x2 + 1, $y2, $red);


            $x21 = $x22;
            $x22 = $x21 + (($graphWidth) / ($cnt - 1));
            $y21 = $y22;
            $y22 = $graphHeight - ($DATA[1][$i] - $min) * ($graphHeight) / ($max - $min) + $padding;
            ImageLine($im, $x21, $y21, $x22, $y22, $purple);
            ImageLine($im, $x21 + 2, $y21 +1, $x22 + 2, $y22 +1, $purple);
        }
        ImageTTFText($im, 8, 0,  ($graphWidth / 2) - 10, $y - 7, $black,
            "./fonts/verdana.ttf", $title);

        $num = rand(1, 100) . rand(1, 100) . rand(1, 100);
        $transTitle = $this->source->Transliterate($title);
        $hash = $transTitle . $num;
        $hash = hash('crc32', $hash);
        $savePath = './img/'. $transTitle . '_' . $hash . '.jpg';
        imagejpeg($im, $savePath);
        return 'https://www.monitoring.novateplo.od.ua/HeatSupplyBot/img/' . $transTitle . '_' . $hash . '.jpg';
    }
}
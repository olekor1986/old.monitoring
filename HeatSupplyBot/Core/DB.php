<?php


/**
 * Class DB
 */
abstract class DB
{
    /**
     * @var PDO
     */
    protected $db;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        try {
            $this->db = new PDO('mysql:host=ok309080.mysql.tools;dbname=ok309080_monitoring',
                            'ok309080_monitoring',
                            '*j!dAM9m95');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->exec('SET NAMES "utf8"');
        } catch (Exception $e) {
            $message = 'DB access error! ' . $e->getMessage();
            die($message);
        }
    }
}
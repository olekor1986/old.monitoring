<?php
require_once "config/db_config.php";

try {
        $sql = "SELECT * FROM objects WHERE objects.name = 'azarova13'";
        $selectObj = $db->prepare($sql);
        $selectObj->execute();
        $objects = $selectObj->fetchAll();
    } catch (Exception $e) {
        $message = 'Error displaying data: ' . $e->getMessage();
        die($message);
    }
$alarmStatus = $objects[0]['alarm'];

if(count($_GET)){
    $chartDataJson = [];
    $errors = [];
    if ($_GET['getData'] == 1){    
        try {
            $sql = "SELECT * FROM water_azarova13 ORDER BY water_azarova13.date DESC LIMIT 2000";
            $selectObj = $db->prepare($sql);
            $selectObj->execute();
            $waterData = $selectObj->fetchAll();
        } catch (Exception $e) {
            $errors = 'Error displaying data: ' . $e->getMessage();
        }

        $waterData = array_reverse($waterData);

        $smallWaterData = [];

        $i = 0;
        foreach ($waterData as $key => $value) {
            if ($value['t1'] < 0 || $value['t2'] < 0){
                continue;
            }
            if (intval($key) % 25 != 0){
                continue;
            }
            $smallWaterData[$i]['date'] = $value['date'];
            $smallWaterData[$i]['p2'] = $value['p2'];
            $smallWaterData[$i]['t1'] = $value['t1'];
            $smallWaterData[$i]['t2'] = $value['t2'];
            $i++;
        }

        $chartDataJson['data'] = $smallWaterData;
        $chartDataJson['errors'] = $errors;
        echo strval(json_encode($chartDataJson));
    } else {
        $chartDataJson['errors'] = 'Invalid request';
        echo strval(json_encode($chartDataJson));
    }
} else {
        try {
        $sql = "SELECT * FROM water_azarova13 ORDER BY water_azarova13.date DESC LIMIT 1";
        $selectObj = $db->prepare($sql);
        $selectObj->execute();
        $array = $selectObj->fetchAll();
    } catch (Exception $e) {
        $message = 'Error displaying data: ' . $e->getMessage();
        die($message);
    }

    $alert = 0;
    if ($alarmStatus == 1) {
        if (intval($array[0]['level']) == 1 || 
            intval($array[0]['t1_max_alert']) == 1 || 
            intval($array[0]['t2_max_alert']) == 1 || 
            intval($array[0]['p1_min_alert']) == 1 || 
            intval($array[0]['p2_min_alert']) == 1 || 
            intval($array[0]['p2_max_alert']) == 1)
        {
            $alert = 1;
        }
    }
    $data = [];
    foreach ($array as $value) {
        $data['t1'] = round($value['t1']);
        $data['t2'] = round($value['t2']);
        $data['p1'] = intval(round($value['p1'], 2) * 100);
        $data['p2'] = intval(round($value['p2'], 2) * 100);
        $data['level'] = intval($value['level']);
        $data['alert'] = intval($alert);
        $data['date'] = $value['date'];
    }

    $json_data = json_encode($data);

    echo strval($json_data);
}
?>
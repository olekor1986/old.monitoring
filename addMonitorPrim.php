<?php
require_once "weatherOdessaLoader.php";
require_once "config/db_config.php";



define('TELEGRAM_TOKEN', '1025369841:AAE0P60FRIGsxoGnniaiwnF4JDetJnEW60M');
//define('TELEGRAM_CHATID', '620175323');
$chatId_1 = '620175323';
//$chatId_2 = '1091130220';
define('T1_LOW', '30');
define('T1_HIGH', '65');
define('TV_LOW', '6');
define('TV_HIGH', '26');
define('EQ_ERR', '!!!---АВАРИЯ ОБОРУДОВАНИЯ---!!!');
define('NORM', '(((ВСЕ В НОРМЕ)))');


function message_to_telegram($text, $chatId)
{
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendMessage',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_POSTFIELDS => array(
                'chat_id' => $chatId,
                'text' => $text,
            ),
        )
    );
    curl_exec($ch);
}

if (!empty($_GET)) {
    foreach ($_GET as $value) {
        if ($value == -127 || $value == 85) {
            die(0);
        }

    }
} else {
    die(0);
}

$k = htmlspecialchars(stripslashes(trim($_GET['k'])), ENT_QUOTES, "UTF-8");


if ($k != 'f7ebf6wv38dk2d7rj8') {
    die(0);
}

$t_1 = htmlspecialchars(stripslashes(trim($_GET['t0'])), ENT_QUOTES, "UTF-8");
$t_2 = htmlspecialchars(stripslashes(trim($_GET['t1'])), ENT_QUOTES, "UTF-8");
$t_v = htmlspecialchars(stripslashes(trim($_GET['t2'])), ENT_QUOTES, "UTF-8");
if ($t_v == $t_1 || $t_v == $t_2 || $t_1 == $t_2) {
    die(0);
}

$t_n = round($currentWeatherOutTemp, 2);

if ($t_n == -273) {
    die(0);
}


$alert = '';
$message = 'T1=' . $t_1 . '°C; ' . "\r\n"
        . 'T2=' . $t_2 . '°C; ' . "\r\n"
        . 'Tв=' . $t_v . '°C; ' . "\r\n"
        . 'Tн=' . $t_n . '°C;';

if ($t_1 < T1_LOW) {
    $alert = "Приморская, 59:" . "\r\n" . EQ_ERR . "\r\n" .
            "Низкая температура подачи  - " . $t_1 . '°C';
    message_to_telegram($alert . "\r\n" . $message, $chatId_1);
} else if ($t_1 > T1_HIGH) {
    $alert = "Приморская, 59:" . "\r\n" . EQ_ERR . "\r\n" .
            "Высокая температура подачи  - " . $t_1 . '°C';
    message_to_telegram($alert . "\r\n" . $message, $chatId_1);
}
if ($t_v < TV_LOW) {
    $alert = "Приморская, 59:" . "\r\n" . EQ_ERR . "\r\n" .
            "Низкая температура помещения  - " . $t_v . '°C';
    message_to_telegram($alert . "\r\n" . $message, $chatId_1);
}
/*
else if ($t_v > TV_HIGH) {
    $alert = "Приморская, 59:" . "\r\n" . EQ_ERR . "\r\n" .
            "Высокая температура помещения  - " . $t_v . '°C';
    message_to_telegram($alert);
    message_to_telegram($message);
}
*/

$date = new DateTime('now');
$data = '';
$data .= $date->format('Y-m-d H:i:s') . ',';
$data .= $t_1 . ',';
$data .= $t_2 . ',';
$data .= $t_v . ',';
$data .= $t_n;
$data .= "\r\n";

$message = 'Приморская, 59: ' . "\r\n" . NORM . "\r\n" .  $message;

if (!file_exists('timelog.txt')) {
    message_to_telegram($message, $chatId_1);
    $timelog = fopen("timelog.txt", "w");
    fwrite($timelog, $date->getTimestamp());
    fclose($timelog);
} else {
    $timestamp = file_get_contents('timelog.txt');
    $timeLast = new DateTime();
    $timeLast = $timeLast->setTimestamp($timestamp);
    $interval = $date->diff($timeLast);
    if ($interval->{'h'} >= 1) {
        message_to_telegram($message, $chatId_1);
        $timelog = fopen("timelog.txt", "w");
        fwrite($timelog, $date->getTimestamp());
        fclose($timelog);
    }
}



$file = fopen("prim59Log20-21.csv", "a");
fwrite($file, $data);
fclose($file);

try {
    $sql = "INSERT INTO monitoring_prim59 SET
            t_1 = :t_1,
            t_2 = :t_2,
            t_v = :t_v,
            t_n = :t_n
            ";
    $storeObject = $db->prepare($sql);
    $storeObject->bindValue(':t_1', $t_1);
    $storeObject->bindValue(':t_2', $t_2);
    $storeObject->bindValue(':t_v', $t_v);
    $storeObject->bindValue(':t_n', $t_n);

    $storeObject->execute();
} catch (Exception $e) {
    $message = 'Error adding data! ' . $e->getMessage();
    die($message);
}

die(1);
?>
<?php
require_once "config/db_config.php";

if (!isset($_GET['num']) || $_GET['num'] == '') {
    $num = 20;
} else {
    $num  = htmlspecialchars(stripslashes(trim($_GET['num'])));
}

try {
    $sql = "SELECT * FROM monitoring_prim59 ORDER BY monitoring_prim59.date DESC LIMIT $num";
    $selectObj = $db->prepare($sql);
    $selectObj->execute();
    $array = $selectObj->fetchAll();
} catch (Exception $e) {
    $message = 'Error displaying data: ' . $e->getMessage();
    die($message);
}

$array = array_reverse($array);

$data = 'date,t1,t2,tv,tn' . "\r\n";
foreach ($array as $key => $value) {
    $data .= $value['date'] . ',';
    $data .= $value['t_1'] . ',';
    $data .= $value['t_2'] . ',';
    $data .= $value['t_v'] . ',';
    $data .= $value['t_n'];
    $data .= "\r\n";

}

$file = fopen("chartPrim59.csv", "w");
fwrite($file, $data);
fclose($file);

header("Location:chart.php")
?>
<?php
require_once "config/db_config.php";

define('TELEGRAM_TOKEN', '1025369841:AAE0P60FRIGsxoGnniaiwnF4JDetJnEW60M');
define('EQ_ERR', '!!!---АВАРИЯ ОБОРУДОВАНИЯ---!!!');
define('NORM', '(((ВСЕ В НОРМЕ)))');
define('ADDRESS', 'Азарова, 13');
$condition = true;

try {
        $sql = "SELECT * FROM objects WHERE objects.name = 'azarova13'";
        $selectObj = $db->prepare($sql);
        $selectObj->execute();
        $objects = $selectObj->fetchAll();
    } catch (Exception $e) {
        $message = 'Error displaying data: ' . $e->getMessage();
        die($message);
    }
$alarmStatus = $objects[0]['alarm'];
$chatIds = json_decode($objects[0]['telegram_id'], true);

function message_to_telegram($text, $chatId)
{
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendMessage',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_POSTFIELDS => array(
                'chat_id' => $chatId,
                'text' => $text,
            ),
        )
    );
    curl_exec($ch);
}

if (empty($_GET)){
    /*  ADD PARAMS TO FILE - START  */
        if (empty($_POST)){
            die('{Empty Request}');
        } else {
            if (md5($_POST['code']) == $objects[0]['code']) {
                if (isset($_POST['alarm'])){
                    $alarm = intval(htmlspecialchars(stripslashes(trim($_POST['alarm']))));
                    try {
                        $sql = "UPDATE objects SET
                        alarm = :alarm
                        WHERE name = 'azarova13'";
                        $storeObject = $db->prepare($sql);
                        $storeObject->bindValue(':alarm', $alarm);
                        $storeObject->execute();
                    } catch (Exception $e) {
                        $message = 'Error displaying data: ' . $e->getMessage();
                        die($message);
                    }

                    header('Location: http://monitoring.old/waterAzarova13.php');
                    die();  
                }
                
                $t_max = floatval(htmlspecialchars(stripslashes(trim($_POST['t_max'])), ENT_QUOTES, "UTF-8"));
                $p1_min = floatval(htmlspecialchars(stripslashes(trim($_POST['p1_min'])), ENT_QUOTES, "UTF-8"));
                $p2_min = floatval(htmlspecialchars(stripslashes(trim($_POST['p2_min'])), ENT_QUOTES, "UTF-8"));
                $p2_max = floatval(htmlspecialchars(stripslashes(trim($_POST['p2_max'])), ENT_QUOTES, "UTF-8"));  
                $date = new DateTime('now');

                $dateString = $date->format("Y-m-d H:i:s");
               
                $newLimits = [
                    'date' => $dateString,
                    't_max' => $t_max,
                    'p2_min' => $p2_min,
                    'p2_max' => $p2_max
                ];
               $newLimitsJson = json_encode($newLimits);

                try {
                        $sql = "UPDATE objects SET
                        limits = :limits
                        WHERE name = 'azarova13'";
                        $storeObject = $db->prepare($sql);
                        $storeObject->bindValue(':limits', $newLimitsJson);
                        $storeObject->execute();
                    } catch (Exception $e) {
                        $message = 'Error displaying data: ' . $e->getMessage();
                        die($message);
                    }

                    header('Location: http://monitoring.old/waterAzarova13.php');
                    die();  
                } else {
                    header('Location: http://monitoring.old/waterAzarova13.php');
                    die();
                }
        }
        /*  ADD PARAMS TO FILE - END  */
} else {
    if($_GET['token'] != 'GDA3S580SDNW218765SJ7KDJ21133'){
        die('{Bad token}');
    } else {
        $t1 = floatval(htmlspecialchars(stripslashes(trim($_GET['t1'])), ENT_QUOTES, "UTF-8"));
        $t2 = floatval(htmlspecialchars(stripslashes(trim($_GET['t2'])), ENT_QUOTES, "UTF-8"));
        $p1 = floatval(htmlspecialchars(stripslashes(trim($_GET['p1'])), ENT_QUOTES, "UTF-8"));
        $p2 = floatval(htmlspecialchars(stripslashes(trim($_GET['p2'])), ENT_QUOTES, "UTF-8"));
        $level = intval(htmlspecialchars(stripslashes(trim($_GET['level'])), ENT_QUOTES, "UTF-8"));
    }
}

$limits = json_decode($objects[0]['limits'], true);

$t_max = $limits['t_max'];
$p2_min = $limits['p2_min'];
$p2_max = $limits['p2_max'];

$t1_max_alert = 0;
$t2_max_alert = 0;
$p1_min_alert = 0;
$p2_min_alert = 0;
$p2_max_alert = 0;

$alert = '';
$message = 'T1=' . $t1 . '°C; ' . "\r\n"
        . 'T2=' . $t2 . '°C; ' . "\r\n"
        . 'P1=' . $p1 . 'bar; ' . "\r\n"
        . 'P2=' . $p2 . 'bar; ' . "\r\n"
        . 'Загрузить последние данные: /azarova';
if ($alarmStatus == 1) {
    if ($t1 >= $t_max){
        $t1_max_alert = 1;
        $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
                "Высокая температура насоса №1  - " . $t1 . '°C';
        foreach ($chatIds as $chatId) {
            message_to_telegram($alert . "\r\n" . $message, $chatId);
        }
        $condition = false;
    }

    if ($t2 >= $t_max){
        $t2_max_alert = 1;
            $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
                "Высокая температура насоса №2  - " . $t2 . '°C';
        foreach ($chatIds as $chatId) {
            message_to_telegram($alert . "\r\n" . $message, $chatId);
        }
        $condition = false;
    }

    if ($p2 <= $p2_min){
        $p2_min_alert = 1;
        $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
                "Низкое давление подачи  - " . $p2 . 'bar';
        foreach ($chatIds as $chatId) {
            message_to_telegram($alert . "\r\n" . $message, $chatId);
        }
        $condition = false;
    }

    if ($p2 >= $p2_max){
        $p2_max_alert = 1;
        $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
                "Высокое давление подачи  - " . $p2 . 'bar';
        foreach ($chatIds as $chatId) {
            message_to_telegram($alert . "\r\n" . $message, $chatId);
        }
        $condition = false;
    }

    if ($level == 1){
        $p2_max_alert = 1;
        $alert = ADDRESS . "\r\n" . EQ_ERR . "\r\n" .
                "Аварийный уровень";
        foreach ($chatIds as $chatId) {
            message_to_telegram($alert . "\r\n" . $message, $chatId);
        }
        $condition = false;
    }
}

$date = new DateTime('now');
$data = '';
$data .= $date->format('Y-m-d H:i:s') . ',';
$data .= $t1 . ',';
$data .= $t2 . ',';
$data .= $p1 . ',';
$data .= $p2 . ',';
$data .= $level . ',';
$data .= $t1_max_alert . ',';
$data .= $t2_max_alert . ',';
$data .= $p1_min_alert . ',';
$data .= $p2_min_alert . ',';
$data .= $p2_max_alert;
$data .= "\r\n";

try {
    $file = fopen("water_azarova13_log.csv", "a");
    fwrite($file, $data);
    fclose($file);
} catch (Exception $e) {
    $message = 'Error opening log file! ' . $e->getMessage();
    die('{File error 2}');
}

$message = ADDRESS . "\r\n" . NORM . "\r\n" .  $message;
if ($condition == true){
       if (!file_exists('timelogAz13.txt')) {
        foreach ($chatIds as $chatId) {
        message_to_telegram($message, $chatId);
    }
        $timelog = fopen("timelogAz13.txt", "w");
        fwrite($timelog, $date->getTimestamp());
        fclose($timelog);
    } else {
        $timestamp = file_get_contents('timelogAz13.txt');
        $timeLast = new DateTime();
        $timeLast = $timeLast->setTimestamp($timestamp);
        $interval = $date->diff($timeLast);
        if ($interval->{'h'} >= 1) {
            foreach ($chatIds as $chatId) {
        message_to_telegram($message, $chatId);
    }
            $timelog = fopen("timelogAz13.txt", "w");
            fwrite($timelog, $date->getTimestamp());
            fclose($timelog);
        }
    }
}

try {
    if(!file_exists('water_azarova13_log.csv')){
        $file = fopen("water_azarova13_log.csv", "x");
        $headers = 'date,t1,t2,p1,p2,level,t1_max_alert,t2_max_alert,p1_min_alert,p2_min_alert,p2_max_alert' . "\r\n";
        fwrite($file, $headers);
        fclose($file);
    }
} catch (Exception $e) {
    $message = 'Error first creating log file! ' . $e->getMessage();
    die('{File error 1}');
}


try {
    $sql = "INSERT INTO water_azarova13 SET
            t1 = :t1,
            t2 = :t2,
            p1 = :p1,
            p2 = :p2,
            level = :level,
            t1_max_alert = :t1_max_alert,
            t2_max_alert = :t2_max_alert,
            p1_min_alert = :p1_min_alert,
            p2_min_alert = :p2_min_alert,
            p2_max_alert = :p2_max_alert
            ";
    $storeObject = $db->prepare($sql);
    $storeObject->bindValue(':t1', $t1);
    $storeObject->bindValue(':t2', $t2);
    $storeObject->bindValue(':p1', $p1);
    $storeObject->bindValue(':p2', $p2);
    $storeObject->bindValue(':level', $level);
    $storeObject->bindValue(':t1_max_alert', $t1_max_alert);
    $storeObject->bindValue(':t2_max_alert', $t2_max_alert);
    $storeObject->bindValue(':p1_min_alert', $p1_min_alert);
    $storeObject->bindValue(':p2_min_alert', $p2_min_alert);
    $storeObject->bindValue(':p2_max_alert', $p2_max_alert);

    $storeObject->execute();
} catch (Exception $e) {
    $message = 'Error adding data! ' . $e->getMessage();
    die('{DB error}');
}

die('{Already sended}');


/*
function csv_to_array(string $filename)
    {
        if (!file_exists($filename)){
            throw new Exception("File don't exists");
        }
        if (!is_readable($filename)){
            throw new Exception("Unreadable file");
        }
        $data = [];
        if (($file = fopen($filename, 'r')) !== FALSE){
            $keys = fgetcsv($file, ',');
            while(($row = fgetcsv($file, ',')) !== FALSE){
                $row = array_combine($keys, $row);
                $data = $row;
            }
        }
        return $data;
    }
*/